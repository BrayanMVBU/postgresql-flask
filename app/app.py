from flask import Flask, render_template

app= Flask(__name__)

@app.route('/')
def Index():    
    frutas=['pera','papaya','manzana','platano','maracuya']
    data = {"titulo":"flask python",
            "bienvenida":"buenos dias a todos",
            "frutas":frutas,
            "nro_frutas": len(frutas)
            }
    return render_template('index.html',data=data)
    
    
if __name__ =="__main__":
    app.run(debug=True, port=5000)
    
    
    
     